package com.example.config;


import com.example.json.entity.ConfigEntity;
import com.example.utils.Tools;
import java.io.IOException;

public class Config {
    private  static ConfigEntity configEntity;


    public static ConfigEntity getConfig() {
        return Config.configEntity;
    }

    public static void init(){
        try {
            configEntity = Tools.readJsonFromClassPath("data/config.json", ConfigEntity.class);
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
